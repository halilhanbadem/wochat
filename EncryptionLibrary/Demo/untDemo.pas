unit untDemo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.ExtCtrls,
  Vcl.StdCtrls;

type
  TfrmDemo = class(TForm)
    logo: TImage;
    memStringData: TMemo;
    memHEncData: TMemo;
    edtHEncKey: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    btnEncryption: TButton;
    btnClear: TButton;
    memDHEncData: TMemo;
    memDStringData: TMemo;
    edtDHEncKey: TEdit;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    btnDecryption: TButton;
    btnDClear: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDemo: TfrmDemo;

implementation

{$R *.dfm}

end.
