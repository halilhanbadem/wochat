program EncryptDEMO;

uses
  Vcl.Forms,
  untDemo in 'untDemo.pas' {frmDemo},
  libHEnc in '..\Source\libHEnc.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmDemo, frmDemo);
  Application.Run;
end.
