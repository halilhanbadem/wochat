{ Invokable interface IWoChatService }

unit WoChatServiceIntf;

interface

uses Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns, WebModuleUnit1;

type

  { Invokable interfaces must derive from IInvokable }
  IWoChatService = interface(IInvokable)
  ['{540E58FA-2169-4618-BA73-480C452A1543}']

   function queryUser(const userName, userPass: String): String stdcall;
   function queryFriend(const userID: Integer): String stdcall;
   function queryBlocked(const userID, BlockUserID: Integer): Boolean stdcall;
   function queryToken(const userID: Integer): String stdcall;
   function queryBlockList(const UserID: Integer): String stdcall;
   function queryMessages(const UserID, OtherUserID: Integer): String stdcall;
   function queryForgetPassword(const UserID: Integer): Boolean stdcall;

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IWoChatService));

end.
