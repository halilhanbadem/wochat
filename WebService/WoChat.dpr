program WoChat;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  FormUnit1 in 'FormUnit1.pas' {frmMain},
  WebModuleUnit1 in 'WebModuleUnit1.pas' {WebModule1: TWebModule},
  WoChatServiceImpl in 'WoChatServiceImpl.pas',
  WoChatServiceIntf in 'WoChatServiceIntf.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
