{ Developer Halil Han BADEM }

unit WoChatServiceImpl;

interface

uses Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns, WoChatServiceIntf,
  System.JSON, System.JSON.Writers, WebModuleUnit1;

type

  { TWoChatService }
  TWoChatService = class(TInvokableClass, IWoChatService)
  public
    function queryUser(const userName, userPass: String): String stdcall;
    function queryFriend(const userID: Integer): String stdcall;
    function queryBlocked(const userID, BlockUserID: Integer): Boolean stdcall;
    function queryToken(const userID: Integer): String stdcall;
    function queryBlockList(const userID: Integer): String stdcall;
    function queryMessages(const userID, OtherUserID: Integer): String stdcall;
    function queryForgetPassword(const userID: Integer): Boolean stdcall;
  end;

var
  MainJSON: TJsonObjectWriter;
  WebModule: TWebModule1;

implementation

{ TWoChatService }

function TWoChatService.queryBlocked(const userID,
  BlockUserID: Integer): Boolean;
begin
  // MainJSON := TJsonWriter.Create;
  WebModule := TWebModule1.Create(nil);
  try
    with WebModule.qrySorgu do
    begin
      Close;
      SQL.Clear;
      SQL.Text :=
        'SELECT * FROM blocklist WHERE (userid=:userid) AND (block_userid=:block_userid)';
      ParamByName('userid').AsInteger := userID;
      ParamByName('block_userid').AsInteger := BlockUserID;
      Open;

      if RecordCount > 0 then
      begin
        Result := True;
      end
      else
      begin
        Result := False;
      end;
      Close;
    end;
  finally
     WebModule.Free;
  end;
end;

function TWoChatService.queryBlockList(const userID: Integer): String;
begin
  MainJSON := TJsonObjectWriter.Create;
  WebModule := TWebModule1.Create(nil);
  try
    with WebModule.qrySorgu do
    begin
      Close;
      SQL.Clear;
      SQL.Text :=
        'SELECT blocklist.*, users.id, users.user_name, users.user_name_surname, users.user_last_date FROM blocklist INNER JOIN users ON users.id = blocklist.block_userid WHERE userid=:id';
      ParamByName('id').AsInteger := userID;
      Open;

      if RecordCount > 0 then
      begin
        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('BlockList');
        MainJSON.WriteStartArray;

        First;
        while not Eof do
        begin
          MainJSON.WriteStartObject;
          MainJSON.WritePropertyName('userID');
          MainJSON.WriteValue(FieldByName('block_userid').AsInteger);
          MainJSON.WriteEndObject;
          MainJSON.WriteStartObject;
          MainJSON.WritePropertyName('userName');
          MainJSON.WriteValue(FieldByName('user_name').AsString);
          MainJSON.WriteEndObject;
          MainJSON.WriteStartObject;
          MainJSON.WritePropertyName('userNameSurname');
          MainJSON.WriteValue(FieldByName('user_name_surname').AsString);
          MainJSON.WriteEndObject;
          MainJSON.WriteStartObject;
          MainJSON.WritePropertyName('userLastDate');
          MainJSON.WriteValue(FieldByName('user_last_date').AsDateTime);
          MainJSON.WriteEndObject;
          Next;
        end;

        MainJSON.WriteEndArray;
        MainJSON.WriteEndObject;
        Result := MainJSON.JSON.ToJSON;
      end
      else
      begin
        Result := 'notblock';
      end;
      Close;
    end;
  finally
     MainJSON.DisposeOf;
     WebModule.Free;
  end;
end;

function TWoChatService.queryForgetPassword(const userID: Integer): Boolean;
begin
  MainJSON := TJsonObjectWriter.Create;
  try

  finally
    MainJSON.Free;
  end;
end;

function TWoChatService.queryFriend(const userID: Integer): String;
begin
  MainJSON := TJsonObjectWriter.Create;
  WebModule := TWebModule1.Create(nil);
  try
    with WebModule.qrySorgu do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'select friends.friend_userid,'+
      ' users.id, users.user_name, '+
      ' users.user_mail, users.user_name_surname, '+
      'users.user_last_date, users.user_status, users.user_bio'+
      ' from friends inner join users on users.id = friends.id order by users.user_name_surname asc';
      Open;


      MainJSON.WriteStartObject;
      MainJSON.WritePropertyName('friends');
      MainJSON.WriteStartArray;

      if RecordCount > 0 then
      begin
       First;
       while not Eof do
       begin
        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('friendID');
        MainJSON.WriteValue(FieldByName('users.id').AsInteger);
        MainJSON.WriteEndObject;

        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('friendName');
        MainJSON.WriteValue(FieldByName('users.user_name').AsString);
        MainJSON.WriteEndObject;

        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('friendMail');
        MainJSON.WriteValue(FieldByName('users.user_mail').AsString);
        MainJSON.WriteEndObject;

        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('friendNameSurname');
        MainJSON.WriteValue(FieldByName('users.user_name_surname').AsString);
        MainJSON.WriteEndObject;

        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('friendLastDate');
        MainJSON.WriteValue(FieldByName('users.user_last_date').AsDateTime);
        MainJSON.WriteEndObject;

        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('friendStatus');
        MainJSON.WriteValue(FieldByName('users.user_status').AsString);
        MainJSON.WriteEndObject;

        MainJSON.WriteStartObject;
        MainJSON.WritePropertyName('friendBio');
        MainJSON.WriteValue(FieldByName('users.user_bio').AsString);
        MainJSON.WriteEndObject;
        Next;
       end;

       MainJSON.WriteEndArray;
       MainJSON.WriteEndObject;
       Result := MainJSON.JSON.ToJSON;
      end else
      begin
        Result := 'notfriends';
      end;
    end;
  finally
    MainJSON.DisposeOf;
    WebModule.Free;
  end;
end;

function TWoChatService.queryMessages(const userID,
  OtherUserID: Integer): String;
begin
  MainJSON := TJsonObjectWriter.Create;
  WebModule := TWebModule1.Create(nil);
  try
   with WebModule.qrySorgu do
   begin
     Close;
     SQL.Clear;
     SQL.Text := 'SELECT * FROM messages where (send_userid=:userID) AND (OtherUserID=:otherUserID) order by date asc';
     ParamByName('userID').AsInteger := userID;
     ParamByName('otherUserID').AsInteger := OtherUserID;
     Open;

     if RecordCount > 0 then
     begin
       with MainJSON do
       begin
        WriteStartObject;
        WritePropertyName('messages');
        WriteStartArray;
        First;
        while not Eof do
        begin
          WriteStartObject;
          WritePropertyName('messageID');
          WriteValue(FieldByName('id').AsInteger);
          WriteEndObject;

          WriteStartObject;
          WritePropertyName('messageSenderID');
          WriteValue(FieldByName('send_userid').AsInteger);
          WriteEndObject;

          WriteStartObject;
          WritePropertyName('messageReceiveID');
          WriteValue(FieldByName('receive_userid').AsInteger);
          WriteEndObject;

          WriteStartObject;
          WritePropertyName('messageContent');
          WriteValue(FieldByName('content').AsString);
          WriteEndObject;

          WriteStartObject;
          WritePropertyName('messageDate');
          WriteValue(FieldByName('date').AsDateTime);
          WriteEndObject;
          Next;
        end;

        WriteEndArray;
        WriteEndObject;
        Result := JSON.ToJSON;
       end;
     end else
     begin
       Result := 'notmessage';
     end;
   end;
  finally
   MainJSON.DisposeOf;
   WebModule.Free;
  end;
end;

function TWoChatService.queryToken(const userID: Integer): String;
begin
  WebModule := TWebModule1.Create(nil);
 try
   with WebModule.qrySorgu do
   begin
     Close;
     SQL.Clear;
     SQL.Text := 'select * from token where (userid=:id) AND (active=1)';
     ParamByName('id').AsInteger := userID;
     Open;
     Last;

     if RecordCount > 0 then
     begin
       Result := FieldByName('csrf_token').AsString;
     end else
     begin
       Result := 'nottoken';
     end;
     Close;
   end;
 finally
   WebModule.Free;
 end;
end;

function TWoChatService.queryUser(const userName, userPass: String): String;
begin
  MainJSON := TJsonObjectWriter.Create;
  WebModule := TWebModule1.Create(nil);
  try
    with WebModule.qrySorgu do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'select * from users where (user_name=:name) AND (user_pass=:pass)';
      ParamByName('name').AsString := userName;
      ParamByName('pass').AsString := userPass;
      Open;

      if RecordCount > 0 then
      begin
        with MainJSON do
        begin
          WriteStartObject;
          WritePropertyName('user');
          WriteStartArray;

          while not Eof do
          begin
            WriteStartObject;
            WritePropertyName('id');
            WriteValue(FieldByName('id').AsInteger);
            WriteEndObject;

            WriteStartObject;
            WritePropertyName('userName');
            WriteValue(FieldByName('user_name').AsString);
            WriteEndObject;

            WriteStartObject;
            WritePropertyName('userMail');
            WriteValue(FieldByName('user_mail').AsString);
            WriteEndObject;

            WriteStartObject;
            WritePropertyName('userNameSurname');
            WriteValue(FieldByName('user_name_surname').AsString);
            WriteEndObject;

            WriteStartObject;
            WritePropertyName('userRegDate');
            WriteValue(FieldByName('user_regdate').AsDateTime);
            WriteEndObject;

            WriteStartObject;
            WritePropertyName('userLastDate');
            WriteValue(FieldByName('user_last_date').AsDateTime);
            WriteEndObject;

            WriteStartObject;
            WritePropertyName('userStatus');
            WriteValue(FieldByName('user_status').AsString);
            WriteEndObject;

            WriteStartObject;
            WritePropertyName('userBio');
            WriteValue(FieldByName('user_bio').AsString);
            WriteEndObject;
          end;

          WriteEndArray;
          WriteEndObject;

          Result := JSON.ToJSON;
        end;
      end else
      begin
        Result := 'notuser';
      end;
    end;
  finally
   MainJSON.DisposeOf;
   WebModule.Free;
  end;
end;

initialization

{ Invokable classes must be registered }
InvRegistry.RegisterInvokableClass(TWoChatService);

end.
